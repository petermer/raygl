#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/vec3.hpp>
#include <glm/geometric.hpp>

#include <cmath>
#include <iostream>

using namespace glm;

float terrainFunction(float x, float y){
    return 1.8*sin(0.71*x)*sin(y*0.73f)*exp(y*0.06);
}

bool cast(const vec3 &rorigin, const vec3 &rdir, float &resT)
{
    float delt = 0.01f;
    const float mint = 0.001f;
    const float maxt = 50.f;
    float lastH = 0.0f;
    float lastY = 0.0f;

    for (float t = mint; t < maxt; t += delt) {
        const vec3 p = rorigin + rdir*t;
        const float h = terrainFunction(p.x, p.z);

        if (p.y < h) {
            // linear interpolation of intersection distance
            resT = t - delt + delt*(lastH-lastY)/(p.y-lastY-h+lastH);
            return true;
        }

        // error can be larger at farther distances
        delt = 0.01f*t;
        lastH = h;
        lastY = p.y;
    }

    return false;
}

/* === Approximate normal === */
vec3 getNormal(const vec3 &p)
{
    float eps = 0.001f;
    const vec3 n = vec3(terrainFunction(p.x - eps, p.z) - terrainFunction(p.x + eps, p.z),
                        2.f*eps,
                        terrainFunction(p.x,p.z-eps) - terrainFunction(p.x, p.z+eps));
    return normalize(n);
}

/* === Apply distance fog === */
vec3 fog(const vec3 &rgb, float t)
{
    float fogFactor = exp2(-t*0.08f);
    const vec3 skyColor = vec3(134.f, 215.f, 246.f);
    return mix(skyColor, rgb, fogFactor);
}

vec3 terrainColor(const vec3 &ro, const vec3 &rd, float t)
{
    const vec3 p = ro + rd * t;
    const vec3 n = getNormal(p);
    const vec3 lightDir = normalize(vec3(0, 0.3f, 1.4f));

    float c = clamp(dot(lightDir, n), 0.f, 1.f);

    return fog(c * vec3(255, 0, 0), t);
}

void renderImage(unsigned char *image, int xres, int yres)
{
    const vec3 skyColor = vec3(134.f, 215.f, 246.f);
    #pragma omp parallel for collapse(2)
    for (int j = 0; j<yres; j++) {
        for (int i = 0; i<xres; i++) {
            float t;
            vec3 rayO(0.f, 2.f, 3.f), rayD;
            rayD.x = (float)i/xres - 0.5f;
            rayD.y = -(float)j/yres + 0.5f;
            rayD.z = -1.f;

            // using flipped y coordinates
            // (yres - j) since opengl has
            // the origin on the lower left,
            // while the image has its origin at the
            // upper left
            if (cast(rayO, rayD, t)) {
                vec3 col = terrainColor(rayO, rayD, t);
                image[3*(xres*(yres-j - 1) + i) + 0] = col.x;
                image[3*(xres*(yres-j - 1) + i) + 1] = col.y;
                image[3*(xres*(yres-j - 1) + i) + 2] = col.z;
            } else {
                image[3*(xres*(yres-j - 1) + i) + 0] = skyColor.r;
                image[3*(xres*(yres-j - 1) + i) + 1] = skyColor.g;
                image[3*(xres*(yres-j - 1) + i) + 2] = skyColor.b;
            }

        }
    }
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

int main()
{
    if (!glfwInit())
        return -1;

    GLFWwindow* window = glfwCreateWindow(640, 270, "RayGL:UP", NULL, NULL);

    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);

    // init glew
    if (GLEW_OK != glewInit()) {
        return -1;
    }

    int imageXres = 128*2;
    int imageYres = 64*2;
    unsigned char *imageData = new unsigned char[imageXres*imageYres*3];
    double time = glfwGetTime();
    renderImage(imageData, imageXres, imageYres);
    std::cout << "Rendering finished in " << glfwGetTime() - time << "s." << std::endl;

    glEnable(GL_TEXTURE_2D);
    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                    0,
                    GL_RGB,
                    imageXres,
                    imageYres,
                    0,
                    GL_RGB,
                    GL_UNSIGNED_BYTE,
                    imageData);
    delete[] imageData;

    glClearColor(0.8f, 0, 0, 1.0);
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        glBegin(GL_TRIANGLES);
        glTexCoord2f(0.f, 1.f);
        glVertex3f(-1.f, 1.f, 0.f);
        glTexCoord2f(1.f, 1.f);
        glVertex3f(1.f, 1.f, 0.f);
        glTexCoord2f(1.f, 0.f);
        glVertex3f(1.f, -1.f, 0.f);
        glTexCoord2f(0.f, 1.f);
        glVertex3f(-1.f, 1.f, 0.f);
        glTexCoord2f(1.f, 0.f);
        glVertex3f(1.f, -1.f, 0.f);
        glTexCoord2f(0.f, 0.f);
        glVertex3f(-1.f, -1.f, 0.f);
        glEnd();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteTextures(1, &tex);
    glfwDestroyWindow(window);

    glfwTerminate();

    return 0;
}
