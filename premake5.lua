solution "RayGL"
    configurations {"Debug", "Release"}
    includedirs {"include"}
    libdirs {"lib"}

project "raygl"
    kind "ConsoleApp"
    language "C++"
    
    files { "*.cc" }
    links {
        "glfw3",
        "GLEW",
        "GL",
        "Xrandr",
        "Xxf86vm",
        "pthread",
        "Xi",
        "X11",
        "Xinerama",
        "Xcursor"
    }
    buildoptions {"-fopenmp"}
    linkoptions {"-fopenmp"}

    filter "configurations:Release"
        optimize "Speed"
