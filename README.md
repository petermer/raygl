# RayGL - A terrain raymarcher with live OpenGL preview! #

Bundled libraries include: GLEW, glfw, glm.  Terrain raymarching code from iniqo quilez.

Currently using legacy OpenGL to display a full-window textured quad,
since there are no performance setbacks (and my laptop doesn't support
modern OpenGL anyway :p).

Using premake5 build system to avoid having to maintain makefiles.